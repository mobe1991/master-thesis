% $Id: INF_Poster.tex 7714 2011-08-31 17:34:46Z tkren $
%
% TU Wien - Faculty of Informatics
% poster template
%
% This template is using the beamer document class and beamerposter package, see
% <http://www.ctan.org/tex-archive/macros/latex/contrib/beamer/>
% <http://www.ctan.org/tex-archive/macros/latex/contrib/beamerposter/>
% <http://www-i6.informatik.rwth-aachen.de/~dreuw/latexbeamerposter.php>
%
% For questions and comments send an email to
% Thomas Krennwallner <tkren@kr.tuwien.ac.at>
%

\documentclass[final,hyperref={pdfpagelabels=true}]{beamer}

\usepackage{tabularx}
\usepackage{TUINFPST}
\usetikzlibrary{arrows,backgrounds,calc,positioning,fit,decorations.pathreplacing,arrows.meta,matrix,shapes}

\title[Software Engineering \& Internet Computing]{An Overview of Distributed Big Data Frameworks}
% if you have a long title looking squeezed on the poster, just force
% some distance:
% \title[Computational Intelligence]{%
%   Integration of Conjunctive Queries over \\[0.2\baselineskip]%
%   Description Logics into HEX-Programs %\\[0.2\baselineskip]%
% }
\author[moritz.becker@gmx.at]{Moritz Becker}
\institute[]{%
  Technische Universit{\"a}t Wien\\[0.25\baselineskip]
  Institut f{\"u}r Informationssysteme\\[0.25\baselineskip]
  Arbeitsbereich: Datenbanken und Artificial Intelligence\\[0.25\baselineskip]
  Betreuer: Prof. Dr. Reinhard Pichler
}
\titlegraphic{\includegraphics[height=52mm]{184-2.eps}}
\date[\today]{\today}
\subject{epilog}
\keywords{my kwd1, my kwd2}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display a grid to help align images 
%\beamertemplategridbackground[1cm]

% for crop marks, uncomment the following line
%\usepackage[cross,width=88truecm,height=123truecm,center]{crop}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamercolor{block body}{fg=black,bg=white}
\setbeamercolor{block title}{fg=TuWienBlue,bg=white}

\setbeamertemplate{block begin}{
	\begin{beamercolorbox}{block title}%
		\begin{tikzpicture}%
		\node[draw,rectangle,line width=3pt,rounded corners=0pt,inner sep=0pt]{%
			\begin{minipage}[c][2cm]{\linewidth}
			\centering\textbf{\insertblocktitle}
			\end{minipage}
		};
		\end{tikzpicture}%
	\end{beamercolorbox}
	\vspace*{1cm}
	\begin{beamercolorbox}{block body}%
	}
	
	\setbeamertemplate{block end}{
	\end{beamercolorbox}
	\vspace{2cm}
}

\begin{document}

% We have a single poster frame.
\begin{frame}
  	\begin{columns}[t]
	    % ---------------------------------------------------------%
	    % Set up a column
	    \begin{column}{.45\textwidth}
			\begin{block}{Motivation}
				\begin{itemize}
					\item Amount of data produced by modern society is ever increasing
						$\rightarrow$ challenges for storing and processing data
					\item Novel trends like the internet of things adumbrate a prospectively steeper increase of data volume
					\item Gap between storage capacity and data access speed \\ $\rightarrow$ need for horizontal scaling of data processing to utilize parallel disk access
					\item Shift from specialized systems to large clusters of commodity hardware to improve the economics of data processing
				\end{itemize}
			
%				The ever increasing amount of data that the modern internet society produces poses challenges to corporations and information systems that need to store and process this data. In addition, novel trends like the internet of things even adumbrate a prospectively steeper increase of the data volume than in the past, thereby supporting the relevance of big data. Due to a gap between storage capacity and data access speed the industry has created frameworks for distributed big data processing that allow the horizontal scaling of data processing on large clusters of commodity hardware. By distributing data and computation across multiple nodes, parallel disk reads with a much higher overall throughput become possible compared to accessing data on a single disk.
%				
%				Why do we need big data processing, why does it need to be distributed?
			\end{block}
		\end{column}
		\begin{column}{.45\textwidth}
			\begin{block}{Goals}
				The plethora of technologies that have been developed (cf. Fig. \ref{fig:big-data-tech-overview} for an excerpt) makes the entrance to the field of big data processing increasingly hard. Thus, the goals of this thesis are:
				\begin{itemize}
					\item Provide a broad, graspable overview for newcomers in the field of big data processing that paves the way for further specialization
					\item Identify different types of big data processing and relevant programming models
					\item Identify and describe key technologies and major frameworks both from a software architectural as well as from an application developer's point of view
					\item Provide implementations of example problem statements using each of the discussed frameworks
				\end{itemize}
%				The plethora of technologies that have been developed (see Figure \ref{fig:big-data-tech-overview} for an excerpt) makes the entrance to the field of big data processing increasingly hard. Therefore, the goal of this thesis to give a broad overview of the field that provides the required orientation and knowledge for further specialization as desired. This involves the identification of the different types of big data processing along with the programming models  that have been designed to implement solutions with. Moreover, the thesis should identify key technologies and major frameworks in the field of big data processing and describe their function from a software architectural point of view as well as from an application developer's point of view by illustrating the implementation of example problem statements using each of the discussed frameworks.
			\end{block}
		\end{column}
	\end{columns}
	\begin{columns}[t]
		\begin{column}{.9\textwidth}
			\vspace{3cm}
			\input{../thesis/technology_overview.tex}
			\vspace{3cm}
	    \end{column}
	    % ---------------------------------------------------------%
	    % end the column
	\end{columns}
	\begin{columns}[t]
	    \begin{column}{.3\textwidth}
	    	\begin{block}{Batch processing}
	    		\textbf{MapReduce} programming model
	    		\begin{itemize}
	    			\item Function \emph{Map}: Map input data to key-value pairs
	    			\item Function \emph{Reduce}: Combine key-value pairs with same key into a single output value
	    			\item Chaining of MapReduce jobs allows for complex operations	
	    		\end{itemize}
	    		\bigskip
    			\textbf{Realizations}
    			\begin{itemize}
    				\item Apache Hadoop
    				\item Apache Spark
    			\end{itemize}
	    	\end{block}
		\end{column}
		\begin{column}{.3\textwidth}
			\begin{block}{Graph processing}
				\textbf{Pregel} programming model
				\begin{itemize}
					\item \"Think like a vertex\" - programs describe the behavior of individual vertices
					\item Vertices can send and receive messages and act upon them
					\item Algorithm execution is discretized in supersteps and terminates when all vertices have simultaneously voted to halt
					\item Input graphs are partitioned and distributed across the cluster
				\end{itemize}
				\bigskip
				\textbf{Realizations}
				\begin{itemize}
					\item Apache Giraph
					\item Apache Spark GraphX
				\end{itemize}
			
%				\textbf{Apache Giraph}
%				
%				Graph computations are transformed to MapReduce jobs that are executed on Hadoop.
%
%				\vspace{.5\baselineskip}
%
%				Performs edge-cut based graph partitioning $\rightarrow$ 1 vertex resides at 1 cluster node
%				
%				\textbf{Apache Spark GraphX}
%				
%				Graph computations are transformed to Spark RDD transformations.
%				
%				\vspace{.5\baselineskip}
%				
%				Performs vertex-cut based graph partitioning $\rightarrow$ 1 vertex may reside at multiple cluster nodes
			\end{block}
		\end{column}
		\begin{column}{.3\textwidth}
			\begin{block}{Stream processing}
				\textbf{Challenges}
				\begin{itemize}
					\item Queries on continuous, unpredictable and unbounded streams of data aka continuous queries
					\item Aggregate operations over unbounded streams may require unbounded memory
					\item Fully consistent fault-recovery involves high overhead
					\item The rate in which a stream source produces data may change any time so a stream processing system must deal with sudden load changes
				\end{itemize}
				\bigskip
				\textbf{Realizations}
				\begin{itemize}
					\item Apache Storm
					\item Apache Spark Streaming
				\end{itemize}
				
%				\textbf{Apache Storm}
%				
%				Supports the definition of networks of \emph{spouts} and \emph{bolts} representing stream sources and processing units, respectively.
%				
%				Supports both record-at-a-time and batched processing of streams.
%				
%				\textbf{Apache Spark Streaming}
%				
%				Extends Spark to be utilized for stream processing by creating RDDs from micro-batches over the received stream records.
				
%				Hence record-at-a-time processing is not supported.
			\end{block}
		\end{column}
  	\end{columns}
\end{frame}

\end{document}

%%% Local Variables:
%%% TeX-PDF-mode: t
%%% TeX-debug-bad-boxes: t
%%% TeX-master: t
%%% TeX-parse-self: t
%%% TeX-auto-save: t
%%% reftex-plug-into-AUCTeX: t
%%% End:
