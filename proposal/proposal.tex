% Copyright (C) 2014-2016 by Thomas Auzinger <thomas@auzinger.name>

\documentclass[draft,final]{vutinfth_proposal} % Remove option 'final' to obtain debug information.

% Load packages to allow in- and output of non-ASCII characters.
\usepackage{lmodern}        % Use an extension of the original Computer Modern font to minimize the use of bitmapped letters.
\usepackage[T1]{fontenc}    % Determines font encoding of the output. Font packages have to be included before this line.
\usepackage[utf8]{inputenc} % Determines encoding of the input. All input files have to use UTF8 encoding.

% Extended LaTeX functionality is enables by including packages with \usepackage{...}.
\usepackage{amsmath}    % Extended typesetting of mathematical expression.
\usepackage{amssymb}    % Provides a multitude of mathematical symbols.
\usepackage{mathtools}  % Further extensions of mathematical typesetting.
\usepackage{microtype}  % Small-scale typographic enhancements.
\usepackage[inline]{enumitem} % User control over the layout of lists (itemize, enumerate, description).
\usepackage{multirow}   % Allows table elements to span several rows.
\usepackage{booktabs}   % Improves the typesettings of tables.
\usepackage{subcaption} % Allows the use of subfigures and enables their referencing.
\usepackage[ruled,linesnumbered,algochapter]{algorithm2e} % Enables the writing of pseudo code.
\usepackage[usenames,dvipsnames,table]{xcolor} % Allows the definition and use of colors. This package has to be included before tikz.
\usepackage{nag}       % Issues warnings when best practices in writing LaTeX documents are violated.
\usepackage{todonotes} % Provides tooltip-like todo notes.
\usepackage{hyperref}  % Enables cross linking in the electronic document version. This package has to be included second to last.
\usepackage[acronym,toc]{glossaries} % Enables the generation of glossaries and lists fo acronyms. This package has to be included last.

% Define convenience functions to use the author name and the thesis title in the PDF document properties.
\newcommand{\authorname}{Moritz Becker} % The author name without titles.
\newcommand{\thesistitle}{Distributed Big Data Frameworks: A Survey} % The title of the thesis. The English version should be used, if it exists.
\newcommand{\curriculum}{Software Engineering \& Internet Computing}

% Set PDF document properties
\hypersetup{
    linkbordercolor = {Melon},                % The color of the borders of boxes around crosslinks (optional).
    pdfauthor       = {\authorname},          % The author's name in the document properties (optional).
    pdftitle        = {\thesistitle},         % The document's title in the document properties (optional).
}

\setpnumwidth{2.5em}        % Avoid overfull hboxes in the table of contents (see memoir manual).
\setsecnumdepth{subsection} % Enumerate subsections.

\nonzeroparskip             % Create space between paragraphs (optional).
\setlength{\parindent}{0pt} % Remove paragraph identation (optional).

%\makeindex      % Use an optional index.
%\makeglossaries % Use an optional glossary.
%\glstocfalse   % Remove the glossaries from the table of contents.

% Set persons with 4 arguments:
%  {title before name}{name}{title after name}{gender}
%  where both titles are optional (i.e. can be given as empty brackets {}).
\setauthor{}{\authorname}{Bsc}{male}
\setadvisor{Prof. Dr.}{Reinhard Pichler}{}{male}

% For bachelor and master theses:
%\setfirstassistant{Pretitle}{Forename Surname}{Posttitle}{male}
%\setsecondassistant{Pretitle}{Forename Surname}{Posttitle}{male}
%\setthirdassistant{Pretitle}{Forename Surname}{Posttitle}{male}

% Required data.
\setaddress{Address}
\setregnumber{1026241}
\setdate{03}{03}{2017} % Set date with 3 arguments: {day}{month}{year}.
\settitle{Proposal}{Proposal} % Sets English and German version of the title (both can be English or German).
\setsubtitle{\thesistitle}{} % Sets English and German version of the subtitle (both can be English or German).

% Select the thesis type: bachelor / master / doctor / phd-school.
% Bachelor:
\setthesis{master}
%
% Master:
%\setthesis{master}
\setmasterdegree{dipl.} % dipl. / rer.nat. / rer.soc.oec. / master
%
% Doctor:
%\setthesis{doctor}
%\setdoctordegree{rer.soc.oec.}% rer.nat. / techn. / rer.soc.oec.
%
% Doctor at the PhD School
%\setthesis{phd-school} % Deactivate non-English title pages (see below)

% For bachelor and master:
\setcurriculum{\curriculum}{\curriculum} % Sets the English and German name of the curriculum.

\begin{document}

\frontmatter % Switches to roman numbering.
% The structure of the thesis has to conform to
%  http://www.informatik.tuwien.ac.at/dekanat

%\addtitlepage{naustrian} % German title page (not for dissertations at the PhD School).
\addtitlepage{english} % English title page.

% Select the language of the thesis, e.g., english or naustrian.
\selectlanguage{english}

\mainmatter

% Switch to arabic numbering and start the enumeration of chapters in the table of content.

\chapter{Problem definition}

Over a decade ago and shortly after presenting their Google File System (GFS) \cite{ghemawat2003google}, Google published the pioneering paper about the MapReduce programming model \cite{dean2010mapreduce}. At that time, IT companies were starting to struggle with the opening gap between the ever larger amounts of data that demanded processing and the developments in hardware that could not keep the pace. In the search for cost effective ways to handle the loads, the industry was aiming for ways to scale their software horizontally to make use of low cost commodity hardware clusters. The revolutionary aspect of Google's MapReduce approach is conceptual simplicity and its generalizability to a wide range of problems. This is also the main reason why it sparked a whole new field of industry and research - distributed big data processing, which, for the sake of compactness, will be referred to as \emph{big data} for the remainder of this proposal.

Since then, a plethora of both proprietary and open technologies and frameworks have been developed based on the MapReduce model but also new programming models have been introduced for problem domains where MapReduce is not well adoptable. The industry's increasing focus on the Internet of Things (IoT) market and the resulting demand for systems capable of processing this type of data at large scale is yet another reason that has fueled investments and developments in big data even further. This mass of research, industry and entrepreneurial efforts makes it hard to establish orientation when entering the field.

{\let\clearpage\relax\chapter{Expected results}}

The proposed thesis aims to provide insights to nowadays most important and widespread big data frameworks in the following fields of application:
\begin{itemize}
	\item Batch processing
	\item Graph processing
	\item Stream processing
	\item Machine learning
\end{itemize}

The thesis will also provide theoretical analyses of the underlying programming models as well as comparisons and technical evaluations of the presented systems.

{\let\clearpage\relax\chapter{Methodology and approach}}

Extensive research in literature from both academia and industry will be conducted and the extracted information will be condensed and uniformly presented. Morever, empiric experiments and benchmarks will be developed and applied in order to objectively compare the systems under consideration.

The introductory part of the thesis is going to revisit the history and milestones of big data frameworks and the challenges they are facing. After that, the fields of application described earlier are going to be examined in sequence in separate chapters.

Each chapter sets in with a theoretical analysis of the application field's particularities, challenges and the theoretical models that have been proposed for each field. Finally, selected big data frameworks for each respective field are described in detail before the conducted experiments and benchmarks are presented.

{\let\clearpage\relax\chapter{State of the art}}

Apache Hadoop consists of a MapReduce implementation, a resource management and scheduling component called YARN and the Hadoop Distributed File System (HDFS) \cite{hadoop}. It has played an important role in big data processing since the early days. Many more high level systems are built upon the HDFS to provide non-functional requirements like fault-tolerance and resilience.

The rise of IoT has fueled demand for stream processing frameworks that focus on the continuous processing, aggregation and analysis of high-volume sensor data that flows into backend systems. One of the widely adopted systems to handle large-scale streaming data is Apache Storm \cite{storm}.

In 2010, Google presented a vertex-centric approach to distributed graph processing called Pregel \cite{malewicz2010pregel}. One popular representative of this class of big data frameworks is Apache Giraph \cite{giraph} which is essentially an open source clone of the proprietary Pregel. Because the vertex-centric abstraction is not trivially applicable to all kinds of graph algorithms, a complementing graph-centric model has been suggested \cite{tian2013think}.

The most recent star among the big data frameworks is Apache Spark which claims to increase processing speed by orders of magnitude for iterative MapReduce jobs compared to competing products by keeping the processed datasets in memory accross iterations \, \cite{zaharia2010spark}. Also, Spark is the only framework that provides built-in support for all of the 4 mentioned fields of application.

{\let\clearpage\relax\chapter{Relation to \curriculum}}

Distributed systems are represented as a separate subject of examination with multiple modules in the \curriculum \, curriculum \cite{curriculum} and can thus be considered an essential part of the studies. Moreover, numerous courses exist that conduct teaching on this subject, for example:
\begin{itemize}
	\item 184.269 \textit{Advanced Internet Computing}
	\item 184.267 \textit{Advanced Distributed Systems}
	\item 184.271 \textit{Large-scale Distributed Computing}
	\item 184.742 \textit{Advanced Services Engineering}
\end{itemize}

As the proposed topic semantically makes up a subdomain of distributed systems, its relevance is indisputable.

%\backmatter

% Rename bibliography heading
\renewcommand{\bibname}{References}
% Disable page break before bibliography
\renewcommand{\bibsection}{%
	{\let\clearpage\relax\chapter*{\bibname}}
	\prebibhook
}

\bibliographystyle{alpha}
\bibliography{proposal}

\end{document}