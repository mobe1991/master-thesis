rem Copyright (C) 2014-2016 by Thomas Auzinger <thomas@auzinger.name>

@echo off
set CLASS=vutinfth_proposal
set SOURCE=example
@echo on

rem Build the vutinfth class file
pdflatex %CLASS%.ins

@echo off
echo.
echo.
echo Class file and example document compiled.
pause
