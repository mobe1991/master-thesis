Spark was started by Matei Zaharia at the University of California Berkeley in 2009 and presented in a research paper in 2010\cite{zaharia2010spark}. In 2013, the project was donated to the Apache Software Foundation and it became a top-level project in 2014.

The motivation for creating Spark was to better support MapReduce algorithms that reuse a working set of data across multiple parallel operations. For example, this includes any algorithm using iterations and is often the case in machine learning. Another problem in traditional MapReduce implementations like Hadoop is the high latency of operations which is impractical for a range of applications like ad-hoc interactive analytics or serving web client requests. The reason for this is the invocation of a new MapReduce job for each request which potentially needs to load data from across a cluster every time \cite{zaharia2010spark}.

The basic primitive introduced by Spark to alleviate these shortcomings is called a resilient distributed dataset (RDD). It represents a read-only object collection that can be distributed in partitioned form across a cluster and can be cached in memory to be reused in multiple parallel operations. Availability is achieved via lineage, i.e. enough information is maintained about how a particular RDD has been derived from other RDDs which allows the selective recreation of lost partitions \cite{zaharia2010spark}.

Spark is cluster agnostic, i.e. it does not rely on being able to manage cluster resources itself but relies on a third party cluster manager to integrate with \cite{spark-doc-cluster-managers} \cite{spark-cluster-managers}. This architecture prevents reinventing the wheel and, more importantly, it allows Spark to operate alongside other cluster applications by sharing the cluster manager \cite{zaharia2012resilient}. Spark supports the following managers \cite{spark-doc-cluster-managers} \cite{spark-cluster-managers}:
\begin{itemize}
	\item Spark Standlone\\
	This is the default cluster manager that comes with Spark. It is good for getting started fast or for operating Spark on clusters where no other applications ought to run \cite{spark-cluster-managers}.
	\item Apache Mesos \footnote{\url{mesos.apache.org}}
	\item Hadoop YARN (see Section \ref{sec:yarn})
\end{itemize}

Spark programs are created in the form of \emph{drivers} that create a SparkContext and define the computation using RDDs \cite{zaharia2010spark}. The computation plan gets passed to the job scheduler that transforms it into a task set which, in turn, is passed to a task scheduler that interacts with the cluster manager to allocate resources and to spawn the required task executors \cite{zaharia2012resilient} \cite{spark-task-scheduler}. Figure \ref{fig:sparkexecworkflow} illustrates the execution workflow. While Hadoop spawns a new JVM for each task that is executed on a node, a task in Spark is just a pooled thread in an already running JVM, hence Spark jobs are much more lightweight than Hadoop MapReduce jobs \cite{spark-architecture-blog}.
 
\begin{figure}
	\centering
	\begin{tikzpicture}[>=latex']
		\node (driver-label) {\small Driver Program};
		\node[below=of driver-label.west, anchor=west] (driver-listing) {
			\begin{lstlisting}[basicstyle=\miniscule, language=Java, frame=none]
SparkContext sc = 
new SparkContext(...);
RDD rdd1 = sc.load(..);
RDD rdd2 = sc.load(..);
rdd1.join(rdd2);
			\end{lstlisting}
		};
		\node[draw, fit=(driver-label)(driver-listing)] (driver) {};
		
		
		% lineage graph
		\node[draw, minimum width=2em, minimum height=1em, right=5em of driver] (lin3) {};
		\node[draw, minimum width=2em, minimum height=1em, above left=2em and 0.2em of lin3.north] (lin1) {};
		\node[draw, minimum width=2em, minimum height=1em, above right=2em and 0.2em of lin3.north] (lin2) {};
		\node[draw, minimum width=2em, minimum height=1em, below=of lin3] (lin4) {\tiny Target RDD};
		\node[above=5em of lin3] (lineage-graph-label) {\small Lineage graph};
		\node[above=1em of lineage-graph-label] (lineage-graph-label-container-blocker) {};
		
		\draw[->] (lin1) -- (lin3);
		\draw[->] (lin2) -- (lin3);
		\draw[->] (lin3) -- (lin4);
		
		% DAG
		\node[draw, right=7em of lin3] (map-part-1) {\tiny $p_1$};
		\node[draw, above=1em of map-part-1] (map-part-0) {\tiny $p_0$};
		\node[draw, below=1em of map-part-1] (map-part-2) {\tiny $p_2$};
		\node[draw, fit=(map-part-0)(map-part-2)] (map-container) {};
		
		\node[draw, right=2em of map-part-0] (result-part-0) {\tiny $p_0$};
		\node[draw, right=2em of map-part-1] (result-part-1) {\tiny $p_1$};
		\node[draw, right=2em of map-part-2] (result-part-2) {\tiny $p_2$};
		\node[draw, fit=(result-part-0)(result-part-2)] (result-container) {};
		
		\node[draw, above left=1.5em and 2em of map-part-0.north west, anchor=north east] (rdd1-part-0) {\tiny $p_0$};
		\node[draw, below=1em of rdd1-part-0] (rdd1-part-1) {\tiny $p_1$};
		\node[draw, fit=(rdd1-part-0)(rdd1-part-1)] (rdd1-container) {};
		
		\node[draw, below left=1.5em and 2em of map-part-2.south west, anchor=south east] (rdd2-part-1) {\tiny $p_1$};
		\node[draw, above=1em of rdd2-part-1] (rdd2-part-0) {\tiny $p_0$};
		\node[draw, fit=(rdd2-part-0)(rdd2-part-1)] (rdd2-container) {};
		
		\node at(lineage-graph-label -| map-container) (dag-label) {\small DAG};
		
		\draw[->, shorten >= 4em, shorten <= 1em] (lin3) -- (map-container);
		
		\draw[->] (map-part-0.east) -- (result-part-0.west);
		\draw[->] (map-part-1.east) -- (result-part-1.west);
		\draw[->] (map-part-2.east) -- (result-part-2.west);
		
		\draw[->] (rdd1-part-0.east) -- (map-part-0.west);
		\draw[->] (rdd1-part-0.east) -- (map-part-1.west);
		\draw[->] (rdd1-part-0.east) -- (map-part-2.west);
		
		\draw[->] (rdd1-part-1.east) -- (map-part-0.west);
		\draw[->] (rdd1-part-1.east) -- (map-part-1.west);
		\draw[->] (rdd1-part-1.east) -- (map-part-2.west);
		
		\draw[->] (rdd2-part-0.east) -- (map-part-0.west);
		\draw[->] (rdd2-part-0.east) -- (map-part-1.west);
		\draw[->] (rdd2-part-0.east) -- (map-part-2.west);
		
		\draw[->] (rdd2-part-1.east) -- (map-part-0.west);
		\draw[->] (rdd2-part-1.east) -- (map-part-1.west);
		\draw[->] (rdd2-part-1.east) -- (map-part-2.west);
		
		% Spark Context Container
		\node[draw, fit=(lin1)(lin4)(lineage-graph-label)(lineage-graph-label-container-blocker)(rdd1-container)(rdd2-container)(result-container)] (spark-ctx-container) {};
		\node[below right=0.1em of spark-ctx-container.north west] {\small SparkContext};
		
		\draw[->, shorten >= 2.5em] (driver) -- (lin3);
		
		% Spark Application Container
		\node[draw, fit=(driver)(spark-ctx-container)] (spark-app-container) {};
		\node[below right=0.1em of spark-app-container.north west] {\small Spark Application};
		
		% Cluster Manager
		\node[draw, below=1.5em of spark-app-container] (clustermanager) {\small Cluster Manager (Master)};
		
		% Worker
		\node[draw, fill=white, minimum size=10em, below=1.5em of clustermanager] (worker1) {};
		\node[draw, fill=white, minimum size=10em, below right=0.5em of worker1.north west] (worker2) {};
		\node[draw, fill=white, minimum size=10em, below right=0.5em of worker2.north west] (worker3) {};
		\node[below right=0.1em of worker3.north west] {\small Worker (Slave)};
		
		\node[draw, fill=white, minimum width=7em, minimum height=6em, below right=2em and 1em of worker3.north west] (executor3-1) {};
		\node[draw, fill=white, minimum width=7em, minimum height=6em, below right=0.5em of executor3-1.north west] (executor3-2) {};
		\node[draw, fill=white, minimum width=7em, minimum height=6em, below right=0.5em of executor3-2.north west] (executor3-3) {};
		\node[below right=0.1em of executor3-3.north west] {\small Executor};
		
		\node[draw, fill=white, minimum width=4em, minimum height=2em, below right=2em and 1em of executor3-3.north west] (task3-1) {};
		\node[draw, fill=white, minimum width=4em, minimum height=2em, below right=0.5em of task3-1.north west] (task3-2) {};
		\node[draw, fill=white, minimum width=4em, minimum height=2em, below right=0.5em of task3-2.north west] (task3-3) {\small Task};
		
		\draw[<->] (spark-app-container) -- (clustermanager);
		\draw[<->] (clustermanager) -- (worker1);
	\end{tikzpicture}
	\caption{Apache Spark execution workflow}
	\label{fig:sparkexecworkflow}
\end{figure}

The remainder of this chapter covers RDDs in detail before Spark's job scheduler is described subsequently.
 
\section{Resilient Distributed Dataset (RDD)}
\label{sec:rdd}

RDDs are read-only, partitioned collections of records. They can only be created from
\begin{itemize}
	\item another RDD
	\item data in stable storage
\end{itemize}
by applying a set of predefined operations, called \emph{transformations}, like \emph{map}, \emph{filter} or \emph{join} \cite{zaharia2012resilient}.

The partitions of an RDD can be distributed across a cluster, thus allowing different nodes to operate on distinct fractions of the data. Since RDDs are designed to be held in memory and to be evaluated lazily, mechanisms are required to cope with lost partitions due to failures of nodes holding a partition in memory \cite{zaharia2012resilient}.

This is done by ensuring that every partition of an RDD is independently recomputable if necessary.
Part of the information stored in any RDD describes how this RDD has been constructed. This property is also called \emph{lineage} and includes a set of dependencies - other RDDs - and a function for computing the current RDD from its dependencies. Spark distinguishes between two types of dependencies \cite{zaharia2012resilient}:
\begin{itemize}
	\item \emph{Narrow} dependency: Each partition of the parent RDD is used by at most one partition of the child RDD
	\item \emph{Wide} dependency aka shuffle dependency: Partitions of the parent RDD are used in multiple child partitions
\end{itemize}
Dependency types can be used to classify transformations into narrow or wide transformations depending on what kind of dependencies a transformation introduces. For example, \emph{map} transformations lead to narrow dependencies whereas shuffle style operations like \emph{reduce} result in wide dependencies (see Figure \ref{fig:rdddependencytypes}). The distinction of dependency types has an important impact on planning and scheduling the computation of an RDD because narrow transformations are considered eligible for pipelining by Spark whereas wide transformations are treated as pipeline breakers \cite{zaharia2012resilient} \cite{spark-architecture-and-internals}. The details of the scheduling process are discussed in Section \ref{sec:sparkjobscheduler}.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\node at (0, 0) (label-narrow) {Narrow};
		
		\node[draw, minimum width=4em, minimum height=8em, below left=2em and 0em of label-narrow](rdd1) {};
		\node[below right=0.1em of rdd1.north west] {$\text{RDD}_0$};
		
		\node[draw, minimum size=1em, below=1.9em of rdd1.north] (rdd1-1) {};
		\node[draw, minimum size=1em, below=1em of rdd1-1] (rdd1-2) {};
		\node[draw, minimum size=1em, below=1em of rdd1-2] (rdd1-3) {};
		
		\node[draw, minimum width=4em, minimum height=8em, below right=2em and 0em of label-narrow] (rdd2) {};
		\node[below right=0.1em of rdd2.north west] {$\text{RDD}_1$};
		
		\node[draw, minimum size=1em, below=1.9em of rdd2.north] (rdd2-1) {};
		\node[draw, minimum size=1em, below=1em of rdd2-1] (rdd2-2) {};
		\node[draw, minimum size=1em, below=1em of rdd2-2] (rdd2-3) {};
		
		\draw[->] (rdd1-1) -- (rdd2-1) node[midway, above=0.5em] {\small Map};
		\draw[->] (rdd1-2) -- (rdd2-2);
		\draw[->] (rdd1-3) -- (rdd2-3);
		
		\node[right=10em of label-narrow] (label-wide) {Wide};
		
		\node[draw, minimum width=4em, minimum height=8em, below left=2em and 0em of label-wide] (rdd3) {};
		\node[below right=0.1em of rdd3.north west] {$\text{RDD}_0$};
		
		\node[draw, minimum size=1em, below=1.9em of rdd3.north] (rdd3-1) {};
		\node[draw, minimum size=1em, below=1em of rdd3-1] (rdd3-2) {};
		\node[draw, minimum size=1em, below=1em of rdd3-2] (rdd3-3) {};
		
		\node[draw, minimum width=4em, minimum height=8em, below right=2em and 0em of label-wide] (rdd4) {};
		\node[below right=0.1em of rdd4.north west] {$\text{RDD}_1$};
		
		\node[draw, minimum size=1em, below=1.9em of rdd4.north, shift=($(rdd3-2.north)-(rdd3-1.south)$)] (rdd4-1) {};
		\node[draw, minimum size=1em, below=1em of rdd4-1] (rdd4-2) {};
		
		\draw[->] (rdd3-1) -- (rdd4-1) node[midway, above=0.5em] {\small Reduce};
		\draw[->] (rdd3-1) -- (rdd4-2);
		\draw[->] (rdd3-2) -- (rdd4-1);
		\draw[->] (rdd3-2) -- (rdd4-2);
		\draw[->] (rdd3-3) -- (rdd4-1);
		\draw[->] (rdd3-3) -- (rdd4-2);
	\end{tikzpicture}
	\caption{RDD dependency types}
	\label{fig:rdddependencytypes}
\end{figure}

The dependencies between RDDs result in a directed acyclic graph, the \emph{lineage} graph, that allows every RDD and each of its partitions to be transitively recomputed based on data in stable storage \cite{zaharia2012resilient}. See Figure \ref{fig:lineagegraph} for an illustration of the lineage graph for a slightly extended instance of the word count problem. The basic word count problem assumes a given text corpus and the task is to count the number of appearances of each distinct word in the corpus. To make the example more interesting, the complexity of the problem is slightly raised by reading the corpus from two input files rather than one and by applying some sort of filtering on sentence level prior to counting.

\begin{figure}
	\centering
	
	\tikzstyle{container} = [draw, minimum width=8.5em]
	\begin{tikzpicture}
		\node[container] (sentences-joined) {joined sentences};
		\node[right=1em of sentences-joined] {$\text{RDD}_4$};
		
		% left branch
		\node[container, above left=2em and 1em of sentences-joined] (sentences-filtered1) {$\text{filtered sentences}_1$};
		\node[right=1em of sentences-filtered1] {$\text{RDD}_1$};
		
		\node[container, above=of sentences-filtered1] (sentences1) {$\text{sentences}_1$};
		\node[right=1em of sentences1] {$\text{RDD}_0$};
		
		\node[container, above=of sentences1] (corpus1) {corpus1.txt};
		\node[right=1em of corpus1] {Storage};
		
		% right branch
		\node[container, above right=2em and 1em of sentences-joined] (sentences-filtered2) {$\text{filtered sentences}_2$};
		\node[right=1em of sentences-filtered2] {$\text{RDD}_3$};
		
		\node[container, above=of sentences-filtered2] (sentences2) {$sentences_2$};
		\node[right=1em of sentences2] {$\text{RDD}_2$};
		
		\node[container, above=of sentences2] (corpus2) {corpus2.txt};
		\node[right=1em of corpus2] {Storage};
		
		\node[container, below=of sentences-joined] (words) {words};
		\node[right=1em of words] {$\text{RDD}_5$};
		\node[container, below=of words] (word-counts) {word counts};
		\node[right=1em of word-counts] {$\text{RDD}_6$};
		
		\draw[->] (corpus1) -- (sentences1) node[midway, left=0.5em] {load};
		\draw[->] (sentences1) -- (sentences-filtered1) node[midway, left=0.5em] {filter};
		\draw[->] (sentences-filtered1) -- (sentences-joined) node[midway, left=0.8em] {join};
		\draw[->] (corpus2) -- (sentences2) node[midway, left=0.5em] {load};
		\draw[->] (sentences2) -- (sentences-filtered2) node[midway, left=0.5em] {filter};
		\draw[->] (sentences-filtered2) -- (sentences-joined);
		\draw[->] (sentences-joined) -- (words) node[midway, left=0.5em] {map};
		\draw[->] (words) -- (word-counts) node[midway, left=0.5em] {reduce};
	\end{tikzpicture}
	\caption{Lineage with RDDs}
	\label{fig:lineagegraph}
\end{figure}

The left branch of the lineage graph represents the input from the first file named \emph{corpus1.txt} in this example and the right branch stands for the input from the second file named \emph{corpus2.txt}. $\text{RDD}_0$ and $\text{RDD}_2$ are the root RDDs created from the sentence-tokenized contents of the respective files. Whenever either of these RDDs is lost they can be recreated by reading in the input files again. Clearly, this assumes that the corpus data is stored on a reliable storage such as HDFS. $\text{RDD}_1$ and $\text{RDD}_3$ are created by applying a filtering operation on each of the two sentence RDDs. The filtered RDDs are then joined creating another RDD before a map and a reduce operation is performed to produce the final word count result which, again, is represented as RDD. 

Since the lineage graph keeps track of an RDD's parent and the operation that has been applied to the parent to create the RDD, it is possible to recompute an RDD whenever needed given that the parent RDD is still available. If this is not the case, the recovery procedure needs to follow the lineage graph backwards until an available RDD is discovered and recompute the whole chain of RDDs. For example, when $\text{RDD}_5$ and $\text{RDD}_4$ are lost the recovery procedure follows the lineage back until it encounters $\text{RDD}_1$ and $\text{RDD}_3$ that are still available and recomputes $\text{RDD}_4$ by reapplying the join operation. Once $\text{RDD}_4$ has been recovered, $\text{RDD}_5$ can be reconstructed from it.

This lineage approach can unfold its full potential when used in a pipeline of successive transformations. In such a scenario, systems like Hadoop, for example, need to create checkpoints or write results back to HDFS after each step. This not only makes a successive read from disk necessary to retrieve the input for the next transformation but it also incurs overhead for creating replications. In contrast, due to the independent re-computability of RDD partitions there is no need to immediately write to disk. This is clearly the most important benefit the RDDs provide since it helps to greatly speed up the process \cite{zaharia2012resilient} \cite{spark-architecture-and-internals}.

The partitioning strategy of an RDD is configurable by the user. For example, this can be used to apply data locality optimizations in case of a join transformation of two RDDs by ensuring that two partitions getting joined reside on the same node \cite{zaharia2012resilient}.

The user can also influence the persistence of RDDs which is useful in case the same RDD is reused across multiple passes to circumvent the need of recomputing it each time. For this purpose, Spark provides several persistence strategies like in-memory or on-disk persistence or mixtures of both \cite{zaharia2012resilient}.

Finally, RDDs support operations called \emph{actions} that trigger the computation and materialize the results \cite{spark-architecture-and-internals}.

Spark provides specialized RDD implementations for data sources such as HDFS, for example. As shown in Figure \ref{fig:sparkhdfs}, this type of RDD establishes a 1:1 mapping between HDFS blocks and RDD partitions, thus providing optimal data locality properties when working with datasets stored in HDFS \cite{zaharia2012resilient} \cite{spark-architecture-and-internals}.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\node[draw] (hdfs-0) {$\text{block}_0$};
		\node[draw, below=1em of hdfs-0] (hdfs-1) {$\text{block}_1$};
		\node[draw, below=1em of hdfs-1] (hdfs-2) {$\text{block}_2$};
		\node[draw, fit=(hdfs-0)(hdfs-2)] (hdfs-container) {};
		\node[above=1em of hdfs-container.north west, anchor=west] {HDFS};
		
		\node[draw, right=of hdfs-0] (rdd-0) {$p_0$};
		\node[draw, right=of hdfs-1] (rdd-1) {$p_1$};
		\node[draw, right=of hdfs-2] (rdd-2) {$p_2$};
		\node[draw, fit=(rdd-0)(rdd-2)] (rdd-container) {};
		\node[above=1em of rdd-container.north west, anchor=west] {RDD};
		
		\draw[->] (hdfs-0) -- (rdd-0);
		\draw[->] (hdfs-1) -- (rdd-1);
		\draw[->] (hdfs-2) -- (rdd-2);
	\end{tikzpicture}
	\caption{RDD implementation optimized for HDFS}
	\label{fig:sparkhdfs}
\end{figure}


\section{Job scheduler}
\label{sec:sparkjobscheduler}
A \emph{job} in Spark is submitted by the SparkContext to the job scheduler when an action is called on an RDD in the driver program. It consists of a single target RDD, a placeholder for the end result, and the action that should be executed on the parent of the target in order to actually obtain the result. The job scheduler is part of the driver process. It transforms the lineage graph of the target RDD, also called the logical execution plan, into a physical execution plan, also called DAG for directed acyclic graph, consisting of multiple stages, the physical unit of execution in Spark, each operating on partitions of a single RDD. A stage consists of a set of parallel tasks, one task for each partition of the stage's RDD and each task containing a sequence of narrow transformations. It is valid to view a stage as a set of parallel pipelines where each task executes one pipeline. Pipelining has a positive impact on performance since there is no need for storing intermediate results between successive pipelined transformations. The stage boundaries are marked by wide transformations, i.e. shuffle operations. The physical execution plan is therefore the result of introducing stage boundaries at wide transformations in the logical execution plan. Figure \ref{fig:sparkdag} illustrates a DAG for the filtered word count lineage graph from Figure \ref{fig:lineagegraph} \cite{zaharia2012resilient} \cite{spark-architecture-and-internals} \cite{spark-internals-jerry-lead}.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\node[draw] (result-0) {$r_0$};
		\node[draw, below=1em of result-0] (result-1) {$r_1$};
		\node[draw, below=1em of result-1] (result-2) {$r_2$};
		\node[draw, fit=(result-0)(result-2)] (result-container) {};
		
		\node[draw, left=of result-0] (count-0) {$p_0$};
		\node[draw, left=of result-1] (count-1) {$p_1$};
		\node[draw, left=of result-2] (count-2) {$p_2$};
		\node[draw, fit=(count-0)(count-2)] (count-container) {};
		
		\node[draw, left=of count-0] (word-0) {$p_0$};
		\node[draw, left=of count-1] (word-1) {$p_1$};
		\node[draw, left=of count-2] (word-2) {$p_2$};
		\node[draw, fit=(word-0)(word-2)] (word-container) {};
		
		\node[draw, left=of word-0] (joined-0) {$p_0$};
		\node[draw, left=of word-1] (joined-1) {$p_1$};
		\node[draw, left=of word-2] (joined-2) {$p_2$};
		\node[draw, fit=(joined-0)(joined-2)] (joined-container) {};
		
		% branch 1
		\node[draw, above left=3em and 2.5em of joined-0.north west, anchor=south east] (filtered-0-0) {$p_0$};
		\node[draw, below=1em of filtered-0-0] (filtered-0-1) {$p_1$};
		\node[draw, below=1em of filtered-0-1] (filtered-0-2) {$p_2$};
		\node[draw, fit=(filtered-0-0)(filtered-0-2)] (filtered-0-container) {};
		
		\node[draw, left=of filtered-0-0] (sent-0-0) {$p_0$};
		\node[draw, left=of filtered-0-1] (sent-0-1) {$p_1$};
		\node[draw, left=of filtered-0-2] (sent-0-2) {$p_2$};
		\node[draw, fit=(sent-0-0)(sent-0-2)] (sent-0-container) {};
		
		\node[draw, left=of sent-0-0] (corp-0-0) {$p_0$};
		\node[draw, left=of sent-0-1] (corp-0-1) {$p_1$};
		\node[draw, left=of sent-0-2] (corp-0-2) {$p_2$};
		\node[draw, fit=(corp-0-0)(corp-0-2)] (corp-0-container) {};
		
		% branch 2
		\node[draw, below left=3em and 2.5em of joined-2.south west, anchor=north east] (filtered-1-2) {$p_2$};
		\node[draw, above=1em of filtered-1-2] (filtered-1-1) {$p_1$};
		\node[draw, above=1em of filtered-1-1] (filtered-1-0) {$p_0$};
		\node[draw, fit=(filtered-1-0)(filtered-1-2)] (filtered-1-container) {};
		
		\node[draw, left=of filtered-1-0] (sent-1-0) {$p_0$};
		\node[draw, left=of filtered-1-1] (sent-1-1) {$p_1$};
		\node[draw, left=of filtered-1-2] (sent-1-2) {$p_2$};
		\node[draw, fit=(sent-1-0)(sent-1-2)] (sent-1-container) {};
		
		\node[draw, left=of sent-1-0] (corp-1-0) {$p_0$};
		\node[draw, left=of sent-1-1] (corp-1-1) {$p_1$};
		\node[draw, left=of sent-1-2] (corp-1-2) {$p_2$};
		\node[draw, fit=(corp-1-0)(corp-1-2)] (corp-1-container) {};
		
		% stages
		\node[draw, dashed, fit=(corp-0-container)(corp-1-container)(word-container)] (stage-0) {};
		\node[above=1em of stage-0.north west, anchor=west] {Stage 0};
		\node[draw, dashed, fit=(count-container)(result-container)] (stage-1) {};
		\node[above=1em of stage-1.north west, anchor=west] {Stage 1};
		
		\draw[->] (corp-0-0) -- (sent-0-0);
		\draw[->] (corp-0-1) -- (sent-0-1);
		\draw[->] (corp-0-2) -- (sent-0-2);
		\draw[->] (sent-0-0) -- (filtered-0-0);
		\draw[->] (sent-0-1) -- (filtered-0-1);
		\draw[->] (sent-0-2) -- (filtered-0-2);
		
		\draw[->] (corp-1-0) -- (sent-1-0);
		\draw[->] (corp-1-1) -- (sent-1-1);
		\draw[->] (corp-1-2) -- (sent-1-2);
		\draw[->] (sent-1-0) -- (filtered-1-0);
		\draw[->] (sent-1-1) -- (filtered-1-1);
		\draw[->] (sent-1-2) -- (filtered-1-2);
		
		\draw[->] (filtered-0-0.east) -- (joined-0.west);
		\draw[->] (filtered-1-0.east) -- (joined-0.west);
		\draw[->] (filtered-0-1.east) -- (joined-1.west);
		\draw[->] (filtered-1-1.east) -- (joined-1.west);
		\draw[->] (filtered-0-2.east) -- (joined-2.west);
		\draw[->] (filtered-1-2.east) -- (joined-2.west);
		
		\draw[->] (joined-0) -- (word-0);
		\draw[->] (joined-1) -- (word-1);
		\draw[->] (joined-2) -- (word-2);
		
		\draw[->] (word-0.east) -- (count-0.west);
		\draw[->] (word-0.east) -- (count-1.west);
		\draw[->] (word-0.east) -- (count-2.west);
		\draw[->] (word-1.east) -- (count-0.west);
		\draw[->] (word-1.east) -- (count-1.west);
		\draw[->] (word-1.east) -- (count-2.west);
		\draw[->] (word-2.east) -- (count-0.west);
		\draw[->] (word-2.east) -- (count-1.west);
		\draw[->] (word-2.east) -- (count-2.west);
		
		\draw[->] (count-0) -- (result-0);
		\draw[->] (count-1) -- (result-1);
		\draw[->] (count-2) -- (result-2);
	\end{tikzpicture}
	\caption{DAG for lineage graph from Figure \ref{fig:lineagegraph}}
	\label{fig:sparkdag}
\end{figure}

When a job is submitted to the scheduler, it builds the physical execution plan for the job and creates the stages if they do not exist. Whenever possible, the scheduler reuses stages that have been created by other jobs \cite{spark-mastering-spark}. It also tracks which RDDs have been cached on user request and avoids recomputing them. The scheduler then launches tasks to compute missing partitions for each stage until the target RDD has been fully computed. This approach of starting at the target RDD guarantees that only such partitions get evaluated that are required for the result \cite{zaharia2012resilient}. Figure \ref{fig:sparklazy} illustrates this effect using the example of a spark program that joins two RDDs but only returns the first element of the joined RDD. The red lines mark the partitions that are evaluated by Spark to produce this result.

\begin{figure}
	\centering
	\begin{tikzpicture}
		\node[draw] (joined-0) {$p_0$};
		\node[draw, below=1em of joined-0] (joined-1) {$p_1$};
		\node[draw, fit=(joined-0)(joined-1)] (joined-container) {};
		
		\node[draw, right=of joined-container] (result-0) {r};
		\node[draw, fit=(result-0)] {};
		
		\node[draw, above left=of joined-0.north west, anchor=south east] (rdd-0-0) {$p_0$};
		\node[draw, below=1em of rdd-0-0] (rdd-0-1) {$p_1$};
		\node[draw, fit=(rdd-0-0)(rdd-0-1)] {};
		
		\node[draw, below left=of joined-1.south west, anchor=north east] (rdd-1-1) {$p_1$};
		\node[draw, above=1em of rdd-1-1] (rdd-1-0) {$p_0$};
		\node[draw, fit=(rdd-1-0)(rdd-1-1)] {};
		
		\draw[->, thick, red] (joined-0) -- (result-0) node[midway, above, black] {first};
		
		\draw[->, thick, red] (rdd-0-0.east) -- (joined-0.west);
		\draw[->] (rdd-0-1.east) -- (joined-1.west);
		\draw[->, thick, red] (rdd-1-0.east) -- (joined-0.west);
		\draw[->] (rdd-1-1.east) -- (joined-1.west);
	\end{tikzpicture}
	\caption{Lazy computation of partitions in Spark}
	\label{fig:sparklazy}
\end{figure}
	
A stage might depend on the output of some preceding stage to be available in order to compute its output partitions. When a task fails it is recreated on a different node as long as the dependencies of the task's stage are still available. In case the output of a required stage has been lost, tasks are launched to re-compute the missing partitions \cite{zaharia2012resilient}.

\section{Memory management}
\label{sec:spark-memory-management}

Spark provides different storage or persistence modes for RDDs:
\begin{itemize}
	\item In-memory deserialized
	\item In-memory serialized
	\item On-disk
\end{itemize}
The in-memory deserialized storage is the fastest option because the JVM is able to natively access any requested parts of an object without prior deserialization. Serialized storage reduces the memory footprint but increases the data access time. Spark is also able to transparently spill data to disk when it runs out of memory \cite{zaharia2012resilient}.

For this purpose, Spark employs its own memory management on top of the JVM's heap in order to control the amount of heap space consumed and to decide when to spill data to disk. Originally Spark managed a single static memory area for storage of RDDs but it did not do explicit bookkeeping for temporary memory consumption induced by operations such as joins or aggregations. In practice, this was often the cause for memory exhaustions during memory intensive operations. This shortcoming was resolved by introducing the Unified Memory Manager with Spark 1.6.0 which provides a separate, managed memory area for exactly the purpose of storing temporary data during transformations \cite{DBLP:journals/pvldb/ArmbrustDDGORSW15} \cite{spark-memory-management}. The memory model, as depicted in Figure \ref{fig:spark-memory-model}, allows the two managed memory areas to dynamically borrow memory from each other.

\begin{figure}
	\centering
	
	\tikzstyle{node} = [draw, minimum width=9em, align=center, outer sep=0]
	\tikzstyle{borderless} = [minimum width=9em, align=center, outer sep=0]
	\begin{tikzpicture}
		\node[borderless, minimum height=6em] (storage) {Storage memory};
		\node[borderless, minimum height=6em, below=0em of storage] (execution) {Execution memory};
		\node[node, minimum height=4em, below=0em of execution] (user) {User memory};
		\node[node, minimum height=4em, below=0em of user] (reserved) {Reserved memory};
		
		\draw [decorate,decoration={brace,amplitude=10pt,raise=0.5em}] (storage.north east) -- (reserved.south east) node[midway,xshift=3em] {Heap};
		
		\draw (storage.north west) -- (storage.north east);
		\draw (storage.north west) -- (execution.south west);
		\draw (storage.north east) -- (execution.south east);
		\draw[dashed] (execution.north east) -- (execution.north west);
		\draw[->, shorten <= 4em, thick] (execution.south) -- (execution.north);
		\draw[->, shorten <= 4em, thick] (storage.north) -- (storage.south);
	\end{tikzpicture}
	\caption{Memory model used by Spark's Unified Memory Manager}
	\label{fig:spark-memory-model}
\end{figure}

Both the execution memory and the storage memory are organized in blocks and grow dynamically towards each other.

The \emph{execution memory} holds objects like shuffle buffers, for example, that are required for the execution of spark tasks. It automatically consumes any free space from the storage memory and can also trigger eviction of blocks in the storage memory in case it needs to grow but no free memory is available. Spilling to disk when low on memory is the execution memory's own responsibility and forceful eviction by other tasks is not allowed because the stored data represents intermediate state that is required by ongoing computations.

The \emph{storage memory} can borrow free memory from the execution memory and is used to store RDD data. Eviction is performed according to a least recently used (LRU) policy \cite{zaharia2012resilient}.

The \emph{user memory} can be used by the computations supplied by the driver program to store data. It is not managed by Spark and the user application is responsible for not violating the memory bounds.

A fixed size \emph{reserved memory} space is budgeted for the purpose of running the Spark worker process along with the task executors which also requires some memory to be available. 

\section{Shuffle}

The shuffle operation is performed at stage boundaries and Spark supports two flavors of it:
\begin{itemize}
	\item Hash shuffle
	\item Sort shuffle
\end{itemize}

For the sake of simplicity, the following explanations of the shuffle algorithms rely on the MapReduce naming convention for the two sides of the shuffle, namely the mapper and the reducer. In reality, Spark performs shuffle for a more diverse set of transformations than just MapReduce. 

The hash shuffle is the naive way of performing the shuffle. Each mapper task creates one file for each reducer where it writes the corresponding records to. The algorithm works well for small reducer sizes but in practice, this method is problematic because of the large number of files created. Spark provides an optimization for this approach by pooling output files and reusing them across mapper tasks but this only eases symptoms \cite{spark-shuffle}.

Due to the shortcomings of the hash shuffle, Spark 1.2.0 introduced the sort shuffle which is similar to the algorithm used by Hadoop MapReduce. Records are written \emph{sorted} by reducer id to a single output file per mapper task. By maintaining an index of file offsets for each reducer id, the appropriate chunk can be read quickly when a reducer queries it. Because hashing is generally faster than sorting, there is a threshold on the number of reducers below which the records are hashed to separate files and then merged to a single file \cite{spark-shuffle}.

%\todo{maybe describe Tungsten shuffle}

\section{Air quality threshold monitoring with Spark}
\label{sec:airquality-spark}

This section describes an implementation of the solution to the air quality monitoring problem statement from Section \ref{sec:airquality-mapred} using Apache
Spark.

Listing \ref{lst:airquality-report-spark} shows the really compact implementation of the report generation job using Spark. Line 3 starts by reading in the raw air quality data in textual form which is then parsed to a tuple format by applying the \lstinline|parse| method shown in Listing \ref{lst:airquality-report-spark-parse}. This method also transforms the timestamps to distinct numbers per day that become part of the tuple key. The subsequent reduction operation scheduled in line 5 computes the maximum tuple values per indicator, region and day. Another map operation is then used to perform the threshold comparison with the maximum values per day. Each value is either mapped to 1 or 0 depending on whether it violates the threshold or not. Also the tuple key is changed in this step by dropping the day number. Hence, only the indicator and the region remain as tuple key components. Finally, \lstinline|reduceByKey| sums up the violations per indicator and region. The end results are collected via the \lstinline|collectAsMap| method and printed to the console.

\IfFileExists{SparkRunner.java}{}{\write18{curl https://bitbucket.org/mobe1991/master-thesis-examples/raw/\commit/spark/src/main/java/com/bitlawine/bigdatathesis/examples/spark/SparkRunner.java > SparkRunner.java}}
\lstinputlisting[firstline=25, lastline=37, autodedent=true, language=Java, basicstyle=\footnotesize, numbers=left, label=lst:airquality-report-spark, caption=Air quality violations report with Spark]{SparkRunner.java}

\IfFileExists{SparkRunner.java}{}{\write18{curl https://bitbucket.org/mobe1991/master-thesis-examples/raw/\commit/spark/src/main/java/com/bitlawine/bigdatathesis/examples/spark/SparkRunner.java > SparkRunner.java}}
\lstinputlisting[firstline=40, lastline=54, autodedent=true, language=Java, basicstyle=\footnotesize, numbers=left, label=lst:airquality-report-spark-parse, caption=Air quality tuple parsing]{SparkRunner.java}